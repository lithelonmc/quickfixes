package com.lithelonmc.fixes.listeners;

import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.DataStore;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import org.bukkit.Location;
import org.bukkit.entity.Bee;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class GPListener implements Listener {
    @EventHandler
    public void onBeeDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Bee == false) return;
        Bee bee = (Bee) event.getEntity();
        Location hive = bee.getHive();
        if (hive == null) return;
        Claim claim = getDataStore().getClaimAt(hive, true, null);
        if (claim == null) return;
        Location current = bee.getLocation();
        if (event instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent damage = (EntityDamageByEntityEvent) event;
            if (damage.getDamager() instanceof Player == false) return;
            Player player = (Player) damage.getDamager();
            if (claim.allowAccess(player) == null) return;
            event.setCancelled(true);
        } else if (claim.contains(current, true, false) == false) {
            event.setCancelled(true);
        }
    }

    private DataStore getDataStore() {
        return GriefPrevention.instance.dataStore;
    }
}
