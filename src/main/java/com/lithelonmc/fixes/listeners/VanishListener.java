package com.lithelonmc.fixes.listeners;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class VanishListener implements Listener {
    private final Essentials essentials = JavaPlugin.getPlugin(Essentials.class);

    @EventHandler(ignoreCancelled = true)
    public void onEntityDamage(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            cancelDamageEvent(event.getDamager(), event);
        } else if (event.getEntity() instanceof Player) {
            cancelDamageEvent(event.getEntity(), event);
        }
    }

    private void cancelDamageEvent(Entity entity, EntityDamageEvent event) {
        User user = essentials.getUser(entity);
        if (user == null || user.isVanished() == false) return;
        event.setCancelled(true);
    }
}
