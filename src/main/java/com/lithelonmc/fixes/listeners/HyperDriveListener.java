package com.lithelonmc.fixes.listeners;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;
import xzot1k.plugins.hd.api.events.RandomTeleportEvent;
import xzot1k.plugins.hd.api.events.WarpEvent;

public class HyperDriveListener implements Listener {
    private final Essentials essentials = JavaPlugin.getPlugin(Essentials.class);

    @EventHandler
    public void onRandomTeleport(RandomTeleportEvent event) {
        Player player = event.getPlayer();
        setPlayerLastLocation(player);
    }

    @EventHandler
    public void onWarp(WarpEvent event) {
        Player player = event.getPlayer();
        setPlayerLastLocation(player);
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        setPlayerLastLocation(player);
    }

    private void setPlayerLastLocation(Player player) {
        User user = essentials.getUser(player);
        if (user == null) return;
        user.setLastLocation();
    }
}
