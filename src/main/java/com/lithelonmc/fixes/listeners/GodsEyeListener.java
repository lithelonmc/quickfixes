package com.lithelonmc.fixes.listeners;

import com.google.common.collect.Maps;
import com.lithelonmc.fixes.QuickFixes;
import com.lithelonmc.fixes.utils.CompanionUtils;
import godseye.CheckType;
import godseye.GodsEyeAlertEvent;
import godseye.GodsEyePrePlayerViolationEvent;
import me.astero.companions.filemanager.CompanionDetails;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class GodsEyeListener implements Listener {
    private static final Logger LOGGER = QuickFixes.getInstance().getLogger();
    private static final List<String> FROG_ABILITIES = Arrays.asList("LEAP", "SLOW_FALLING");
    private static final Map<String, Integer> FROG_CHECKS = Maps.newHashMap();

    static {
        FROG_CHECKS.put("FLY-TypeC", 15);
        FROG_CHECKS.put("FLY-TypeD", 15);
        FROG_CHECKS.put("FLY-TypeG", 15);
        FROG_CHECKS.put("MOVEMENT-TypeH", 15);
    }

    @EventHandler
    public void onViolationAlert(GodsEyeAlertEvent event) {
        String player = event.getPlayer().getName();
        CheckType type = event.getCheckType();
        int count = event.getViolationCount();
        String message = String.format("%s is suspected of %s (%s)", player, type, count);
        LOGGER.warning(message);
    }

    @EventHandler
    public void onFrogCompanion(GodsEyePrePlayerViolationEvent event) {
        Player player = event.getPlayer();
        CompanionDetails details = getCompanionDetails(player);
        if (details == null) return;
        if (details.getAbilityList().containsAll(FROG_ABILITIES) == false) return;
        String key = event.getCheckType() + "-" + event.getDetection();
        if (FROG_CHECKS.containsKey(key) == false) return;
        int ignore = FROG_CHECKS.get(key);
        if (event.getViolationCount() < ignore) return;
        event.setCancelled(true);
    }

    private CompanionDetails getCompanionDetails(Player player) {
        String name = CompanionUtils.getCompanionName(player);
        if (name == null) return null;
        return CompanionUtils.getDetails(name);
    }
}
