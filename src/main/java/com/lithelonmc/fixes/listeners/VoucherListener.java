package com.lithelonmc.fixes.listeners;

import de.tr7zw.changeme.nbtapi.NBTItem;
import org.bukkit.Material;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class VoucherListener implements Listener {
    @EventHandler(priority = EventPriority.HIGH)
    public void onInteractEntity(PlayerInteractEntityEvent event) {
        Player player = event.getPlayer();
        EquipmentSlot slot = event.getHand();
        if (slot == null) return;
        ItemStack hand = player.getInventory().getItem(slot);
        if (isVoucher(hand) == false) return;
        if (event.getRightClicked() instanceof ItemFrame) {
            event.setCancelled(false);
        }
    }

    private boolean isVoucher(ItemStack item) {
        if (item == null || item.getType() == Material.AIR) return false;
        return new NBTItem(item).hasKey("voucher");
    }
}
