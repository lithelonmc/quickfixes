package com.lithelonmc.fixes.utils;

import me.astero.companions.CompanionsPlugin;
import me.astero.companions.companiondata.PlayerData;
import me.astero.companions.filemanager.CompanionDetails;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class CompanionUtils {
    private static final CompanionsPlugin COMPANIONS = JavaPlugin.getPlugin(CompanionsPlugin.class);

    public static String getCompanionName(Player player) {
        return PlayerData.instanceOf(player).getActiveCompanionName();
    }

    public static CompanionDetails getDetails(String name) {
        name = name.toLowerCase();
        return COMPANIONS.getFileHandler().getCompanionDetails().get(name);
    }
}
