package com.lithelonmc.fixes;

import com.lithelonmc.fixes.listeners.GPListener;
import com.lithelonmc.fixes.listeners.GodsEyeListener;
import com.lithelonmc.fixes.listeners.HyperDriveListener;
import com.lithelonmc.fixes.listeners.VanishListener;
import com.lithelonmc.fixes.listeners.VoucherListener;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class QuickFixes extends JavaPlugin {
    @Getter
    private static QuickFixes instance;

    public void onEnable() {
        instance = this;
        PluginManager manager = Bukkit.getPluginManager();
        manager.registerEvents(new VoucherListener(), this);
        manager.registerEvents(new HyperDriveListener(), this);
        manager.registerEvents(new VanishListener(), this);
        manager.registerEvents(new GPListener(), this);
        manager.registerEvents(new GodsEyeListener(), this);
    }
}
